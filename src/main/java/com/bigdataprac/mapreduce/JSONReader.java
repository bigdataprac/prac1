package com.bigdataprac.mapreduce;

import org.apache.commons.io.FileUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Iterator;

public class JSONReader {

    public static void main(String[] args) {


        try {
            String file_data = FileUtils.readFileToString(new File("config.json"));
            JSONParser jsonParser = new JSONParser();

            try {
                JSONObject mainObj = (JSONObject)jsonParser.parse(file_data);
                Iterator mainIterator = mainObj.keySet().iterator();
                JSONObject systemObj = null;
                String systemName = null;
                String columnKey = null;
                String columnValue = null;
                while(mainIterator.hasNext()){
                    systemName = (String) mainIterator.next();
                    systemObj = (JSONObject)mainObj.get(systemName);
                    System.out.println(systemName);
                    Iterator columnIterator = systemObj.keySet().iterator();
                    while(columnIterator.hasNext()){
                        columnKey = (String)columnIterator.next();
                        columnValue = (String)systemObj.get(columnKey);
                        System.out.println("\t\t"+columnKey+": "+columnValue);
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
